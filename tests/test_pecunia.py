from pecunia import __version__
from app import create_app, db
from app.models import User
from config import Config


class TestConfig(Config):
    """Custom Config used for tests"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite://"


def test_version():
    """Checks the project version"""
    assert __version__ == "0.0.1"


class TestUserModel():
    """Test Cases for the `app.models.User` model"""

    def setup_class(self):
        """setup for tests

        Creates a flask app and initalises the database
        """

        self.app = create_app(TestConfig)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def teardown_class(self):
        """cleanup from tests

        deltes the database and app
        """

        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_password_setting(self):
        """Tests the ability to set user passwords

        checks that `app.models.User.set_password` works correctly
        """

        u = User(username="Q")
        u.set_password("un1c0rn")
        assert u.check_password("sh4rk") == False
        assert u.check_password("un1c0rn") == True

