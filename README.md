# Pecunia

Pecunia is an (in early development) opensource finance management system written in python.

## Setting up a development environment

### Dependencies

This project uses poetry to manage dependencies.
Follow [poetry's install instructions](https://python-poetry.org/docs/#installation).

To run pecunia locally, you first need the code.
You can either clone the repository with http or ssh.

```bash
git clone https://gitlab.com/mokytis/pecunia
# or
git clone git@gitlab.com:mokytis/pecunia
```

Make sure that you are in the pecunia directory and install the dependencies.

```bash
cd pecunia
poetry install
```

### Environment Variables

You will most likely want to run flask in debug mode.
The easiest way to do this is to create a `.flaskenv` file.

```
# .flashenv
FLASK_DEBUG=true
```

### Database

We can now create the database.

```bash
# stay outside the virtual environment
poetry run flask db upgrade

# or enter the virtual environment first
poetry shell
flask db upgrade
```

### Tests

We can use pytest to run the unit tests.

```bash
# stay outside the virtual environment
poetry run pytest

# or enter the virtual environment first
poetry shell
pytest
```

### Running

```bash
# stay outside the virtual environment
poetry run flask run

# or enter the virtual environment first
poetry shell
flask run
```
