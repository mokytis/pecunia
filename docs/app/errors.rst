Error Handlers
=====

.. automodule:: app.errors.handlers
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
