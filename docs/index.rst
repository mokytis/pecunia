.. Pecunia documentation master file, created by
   sphinx-quickstart on Mon Jun 29 12:50:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Pecunia's documentation!
===================================

.. automodule:: app
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   app/index
   tests
   config



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
