from flask import render_template, flash ,redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse

from app import db
from app.models import User

from app.auth import bp
from app.auth.forms import LoginForm, SignupForm


@bp.route("/login", methods=["GET", "POST"])
def login():
    """User Authenication view"""

    if current_user.is_authenticated:
        return redirect(url_for("main.index"))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("auth.login"))

        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get("next")

        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("main.index")
        return redirect(next_page)
    return render_template("auth/login.html", title="Login", form=form)

@bp.route("/signup", methods=["GET", "POST"])
def signup():
    """User Signup View"""

    if current_user.is_authenticated:
        return redirect(url_for("main.index"))

    form = SignupForm()
    if form.validate_on_submit():
        # Create User
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)

        db.session.add(user)
        db.session.commit()

        flash("Account created successfully")
        return redirect(url_for("auth.login"))
    return render_template("auth/signup.html", title="Sign Up", form=form)

@bp.route("/logout")
def logout():
    """User Logout View"""

    logout_user()
    return redirect(url_for("main.index"))

