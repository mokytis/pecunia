from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from app.models import User


class LoginForm(FlaskForm):
    """User Login Form"""
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Login")


class SignupForm(FlaskForm):
    """User Signup Form"""
    username = StringField("Username", validators=[DataRequired()])
    email = StringField("Email", validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    password_confirm = PasswordField("Confirm Password", validators=[
        DataRequired(), EqualTo("password")
    ])
    submit = SubmitField("Sign Up")

    def validate_username(self, username):
        """Checks to see if a username is already in use

        :param username: Username to validate
        :type username: str

        :raises: `wtforms.validators.ValidationError` when the username is in use
        """

        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Username already in use")

    def validate_email(self, email):
        """Checks to see if an email is already in use

        :param email: Email address to validate
        :type email: str

        :raises: `wtforms.validators.ValidationError` when the email address is in use
        """

        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Email address already in use")
