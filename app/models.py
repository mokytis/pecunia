from app import db, login
from werkzeug.security import generate_password_hash, check_password_hash

from flask_login import UserMixin


@login.user_loader
def load_user(id):
    """Gets a user from a user id

    :param id: uder is
    :type id: int

    :return: user with given id
    :rtype: app.models.User
    """

    return User.query.get(int(id))


class User(UserMixin, db.Model):
    """Model User"""

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)

    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return f"<User {self.username}"

    def set_password(self, password):
        """Updates user password

        Takes a plaintext password, hashes is, and updates the users
        password_hash to be the generated hash

        Uses `werkzeug.security.generate_password_hash`

        :param password: plaintext password to hash
        :type password: str
        """
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        """Validates a given password

        Checks the given password against the hash in the database

        :param password: plaintext password to compare against database entry
        :type password: str

        :return: is the password the same as the users?
        :rtype: bool
        """

        return check_password_hash(self.password_hash, password)
