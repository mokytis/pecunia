from flask import render_template

from app import db
from app.errors import bp


@bp.app_errorhandler(404)
def not_found_error(error):
    """Handles 404 Errors

    This route is used by flask for 404 Errors

    :return: The rendered template of "errors/404.html", with status code 404
    """

    return render_template("errors/404.html"), 404

@bp.app_errorhandler(500)
def internal_server_error(error):
    """Handles 500 Errors

    This route is used by flask for 500 Errors

    :return: The rendered template of "errors/500.html", with status code 500
    """

    return render_template("errors/500.html"), 500
