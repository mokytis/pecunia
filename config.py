import os

base_dir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    """Default config object for flask"""

    SECRET_KEY = (
        os.environ.get("SECRET_KEY") or "development-secret-key-this-is-not-secure"
    )

    SQLALCHEMY_DATABASE_URI = os.environ.get(
            "BASE_URL"
    ) or "sqlite:///" + os.path.join(base_dir, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
